import {BrowserRouter} from 'react-router-dom'

import './App.css';
import Header from './Components/Header/Header'
import Slider from "./Components/Slider/slider1";
import MainPageContainer from "./Components/MainPages/MainPageContainer";


function App() {
  return (
      <BrowserRouter>
          <div className="App">
              <Header/>
              <Slider/>
              <MainPageContainer/>
          </div>
      </BrowserRouter>
  );
}

export default App;
