import React,{useState} from 'react';
import {Link} from 'react-router-dom'
import { BiCart } from 'react-icons/bi';
import { BsSearch } from 'react-icons/bs';
import { FaBars } from 'react-icons/fa';
import { TiTimes } from 'react-icons/ti';
import { GoChevronRight, GoChevronDown } from 'react-icons/go';

import MaratIcon from './icon/logo.png'
import {ItemsHome,ItemsProduct} from './ItemsMenuDropDown/items'






const Header = () => {
    const [click, setClick] = useState(false)
    const [dropDown, setDropDown] = useState(false)
    const handleClick = () => setClick(false)

    const onMouseEnter = () =>{
        if(window.innerWidth < 960){
            setDropDown(false)
        }else{
            setDropDown(true)
        }
    };

    const onMouseLeave = () =>{
        if(window.innerWidth < 960){
            setDropDown(false)
        }else{
            setDropDown(false)
        }
    };


    const  DropDown = ({ItemsHome}) =>{
        return (
            <>
                <ul onClick={handleClick} className={click ? "dropDown-menu clicked" : "dropDown-menu"}>
                    {ItemsHome.map((item,index)=>{
                        return (
                            <li key={index}>
                                <Link className={item.cName} to={item.path} onClick={()=>{setClick(false)}}>
                                    {item.title}
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </>
        )
    }


    return (
       <header>
           <div className="menu">
               <FaBars/>
           </div>
           <div className="logo">
               <img src={MaratIcon} alt=""/>
               <Link to='/'>Marat</Link>
           </div>
           <ul className="nav-menu">
               <li className="nav-item" onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                   <Link onClick={()=>{handleClick()}} to="/">
                       Home <GoChevronRight/>
                   </Link>
                   {dropDown && <DropDown ItemsHome={ItemsHome}/>}
               </li>
               <li className="nav-item"><Link to="/">About Us <GoChevronRight/></Link></li>
               <li className="nav-item"><Link to="/">Products <GoChevronRight/></Link></li>
               <li className="nav-item"><Link to="/">Contacts</Link></li>
               <li>
                   <TiTimes className="menu"/>
               </li>
           </ul>
           <div className="cart-icon">
               <span>0</span>
               <Link to='/cart'>
                   <BiCart/>
               </Link>
           </div>
           <div className="search-icon">
               <Link to='/'>
                   <BsSearch/>
               </Link>
           </div>
       </header>
    );
};

export default Header;