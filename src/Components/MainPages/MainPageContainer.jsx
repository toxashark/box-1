import React from 'react';
import {connect} from "react-redux";
import MainPage from "./MainPage";
import {cartItemsProduct} from "../../State/Redusers/ProductR";

const MainPageContainer = (props) => {
    return (
        <div>
            <MainPage cartItems={props.cartItems} cartItemsProduct={props.cartItemsProduct}/>
        </div>
    );
};

let mapStateToProps= (state) =>{
    return {
        cartItems:state.ProductReducer.cartItems
    }
}

export default connect(mapStateToProps, {cartItemsProduct})(MainPageContainer)
