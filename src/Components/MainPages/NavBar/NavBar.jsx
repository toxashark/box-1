import React from 'react';
import formatCurrency from "./counterPrice";

const NavBar = ({cartItems,increment}) => {
    return (
        <div className="NavBar-container">
            <div className="Navbar-cart">
                <div className="Navbar-cart-title">
                    Cart
                </div>
                <div className="NavBar-cart-text">
                    <div>
                        {cartItems.length === 0 ? <div> No Product in the cart.</div> : <div>You have {cartItems.length} in the cart</div>}
                    </div>
                    <div className="cart">
                        <ul className="items_Product">

                            {cartItems.map(item=>(
                                <li key={item.product._id}>
                                    {/*<div><img src={item.product.image} alt=''/></div>*/}
                                    <div>
                                        <button onClick={()=>{increment(item.product._id)}}>+</button>
                                        <div className="right-price">
                                            {item.count} x {item.product.price} {''}
                                        </div>
                                        <div>{item.product.title}</div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="Navbar-cart">
                <div className="Navbar-cart-title">
                    Categories
                </div>
                <div className="NavBar-cart-text">
                    No Product in the cart.
                </div>
            </div>            <div className="Navbar-cart">
            <div className="Navbar-cart-title">
                Filter
            </div>
            <div className="NavBar-cart-text">
                No Product in the cart.
            </div>
        </div>

        </div>
    );
};

export default NavBar;