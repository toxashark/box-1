import React, {useEffect, useState} from 'react';


import NavBar from "./NavBar/NavBar";
import Products from "./Products/Products";

const MainPage = ({cartItems,cartItemsProduct}) => {

    const [count, setCount] = useState(1)
    const [cart, setCart] = useState([])



    const addToCart = (product) =>{
        const product1 = cartItems.slice()
        let alreadyInCart = false
        product1.forEach(item=>{
            if (item.product._id === product._id) {
                setCount(count + 1)
                alreadyInCart=true
                setCart(product1, count)
                console.log(cart,count)
            }

        });
        if (!alreadyInCart){
            cartItemsProduct({product, count: count})
            setCount(1)
        }
    }


    useEffect(()=>{
        console.log(cartItems)

    },[cartItems])

    return (
        <>
            <div className = 'MainPage-container'>
                <NavBar cartItems={cartItems}/>
                <Products addToCart={addToCart}/>
            </div>
        </>

    );
};

export default MainPage;