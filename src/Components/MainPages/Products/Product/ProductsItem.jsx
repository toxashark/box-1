import React from 'react';
import { BiCart } from 'react-icons/bi';

import testImage from './testImage/3-300x300.jpg'

const ProductsItem = ({product,addToCart}) => {



    return (
        <div className="Product-item">
            <img src={testImage} alt=""/>

            <div className="products-box">
                <h2>{product.title}</h2>
                <p>{product.description}</p>
                <span>${product.price}</span>
            </div>
            <div className="center-btn">
                <button onClick={()=>{addToCart(product)}}><BiCart/>Add to cart</button>
            </div>
        </div>
    );
};

export default ProductsItem;