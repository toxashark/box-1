import React, {useEffect, useState} from 'react';

import data from '../../../data.json'
import ProductsItem from "./Product/ProductsItem";
import * as api from '../../../API/api'



const Products = ({addToCart}) => {
    const products = data.products

    // useEffect(()=>{
    //     setproduct(api.getProduct())
    //     console.log(product1)
    // },[])




    return (
        <>
            <div className="product">
                {products.map(product =>{
                    return <ProductsItem addToCart={addToCart} key={product._id} product={product}/>
                })}
            </div>
        </>
    );
};

export default Products;