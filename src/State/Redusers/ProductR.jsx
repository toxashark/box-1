const CART_ITEMS = 'CART_ITEMS'


let initialState = {
    cartItems:[]
};
const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case CART_ITEMS:
             return {...state, cartItems:[...state.cartItems, action.cartItems]}
        default:
            return state

    }
}
export const cartItemsProduct = (cartItems) =>({type:CART_ITEMS, cartItems})

export default ProductReducer