import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import ProductReducer from "./Redusers/ProductR";




// Функция Redux которая объеденяет все редюсеры, каждый редюсер это объект
let reducersGroup = combineReducers({
    ProductReducer:ProductReducer,
});

const store = createStore(reducersGroup, applyMiddleware(thunkMiddleware));


export default store
